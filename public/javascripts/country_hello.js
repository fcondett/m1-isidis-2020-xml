async function sayHello() {		
	const xml = await get('../xml/countries.xml');
	const country = new URL(location).searchParams.get('country');
	
	expression = "//country[name='" + country + "']/name/@cc";
	
	var nsResolver = xml.createNSResolver( xml.ownerDocument == null ? xml.documentElement : xml.ownerDocument.documentElement);

	cc = xml.evaluate(expression, xml, nsResolver, XPathResult.FIRST_ORDERED_NODE_TYPE, null).singleNodeValue.textContent;
	
	const json = await get("https://fourtonfish.com/hellosalut/?cc=" + cc, 'json');
    const greetings = JSON.parse(json);
    document.querySelector('#say_hello').innerHTML = greetings.hello;

}

sayHello();
