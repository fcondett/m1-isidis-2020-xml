window.onload = async function() {
	const xml = await get('../xml/countries.xml');
	const xsl = await get('../xsl/country_infos.xsl');
	const country = new URL(location).searchParams.get('country');
	
	function display() {
		const proc = new XSLTProcessor();
		proc.importStylesheet(xsl);
		proc.setParameter(null, 'country', country);
		const result = proc.transformToFragment(xml, document);
		while (frag_country_infos.firstChild) {
			frag_country_infos.removeChild(frag_country_infos.firstChild);
		}
		frag_country_infos.appendChild(result);
	}

	display();
}


