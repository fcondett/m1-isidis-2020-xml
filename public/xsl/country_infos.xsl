<?xml version="1.0" encoding="UTF-8"?>

<xsl:stylesheet version="1.0"
                xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

<xsl:param name="country" />

<xsl:template match="/">
	<xsl:apply-templates select="//country[name=$country]" />
</xsl:template>

<xsl:template match="country">
	<h1><xsl:value-of select="name"/>
		<span class="badge badge-light text-uppercase"><xsl:value-of select="name/@cc"/></span>
	</h1>
	
	<hr/>
	<div class="container">
		<div class="row">
			<div class="col pl-0">
				<img src="{flag/@src}" alt="Flag  of {$country}" width="250" height="200" float="left"/>
			</div>
			<div class="col pr-0">
				<p><strong>Capital city: </strong> <xsl:value-of select="capital"/></p> 
				<p><strong>Population: </strong> <xsl:value-of select="format-number(population, '###,###')"/> inhabitants</p>
				<p><strong>Area: </strong> <xsl:value-of select="format-number(area, '###,###')"/> km<sup>2</sup></p>
			</div>
		</div>
	</div>
	
	<hr/>
	<h1>Location on Earth</h1>
	<iframe
		width="100%"
		height="400"
		frameborder="0"
		marginheight="0"
		marginwidth="0"
		src="https://maps.google.com/maps?q={name}&amp;hl=en&amp;output=embed">
	</iframe>
</xsl:template>

</xsl:stylesheet>
